# seesaawiki2markdown

[Seesaa Wiki](https://wiki.seesaa.jp/) 記法の文書を markdown に変換するためのサポートプログラム
春山の使っている記法のみを適当にサポートする

## 使い方

```
nkf -w ~/seesaa.txt  | python3 sw2md.py > markdown.md
```
