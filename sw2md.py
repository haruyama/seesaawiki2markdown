#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import re


def reset_state(state):
    state['in_dl_now'] = False


def dl(line, state):
    m = re.match('^:(.+)[|](.+)$', line)
    if m is None:
        return line

    ret = ''
    in_dl = state.get('in_dl')
    if in_dl is not True:
        ret = '<dl>\n'

    state['in_dl'] = True
    state['in_dl_now'] = True
    return ret + '  <dt>' + m.group(1).strip() + '</dt> <dd>' + m.group(2).strip() + '</dd>'


def terminate_block(line, state):
    in_dl = state.get('in_dl')
    in_dl_now = state.get('in_dl_now')
    if in_dl_now is False and in_dl is True:
        state['in_dl'] = False
        return '</dl>\n' + line
    else:
        return line


def heading(line):
    m = re.match('^([*]+)(.+)$', line)
    if m is None:
        return line

    return '#' * len(m.group(1)) + ' ' + m.group(2).strip()


def ul(line):
    m = re.match('^([-]+)(.+)$', line)
    if m is None:
        return line

    return '    ' * (len(m.group(1)) - 1) + '- ' + m.group(2).strip()


def link(line):
    return re.sub('\[\[(.+?)>(.+?)\]\]', '[\\1](\\2)', line)


def main():
    state = {}
    for line in sys.stdin:
        reset_state(state)
        line = dl(line.strip(), state)
        line = heading(line)
        line = ul(line)
        line = link(line)
        line = terminate_block(line, state)
        print(line)
    reset_state(state)
    line = terminate_block('', state)
    print(line)


if __name__ == "__main__":
    main()
